function [gl_coefficient, gl_score, gl_explained] = gene_level_pca(fc_array,groups,screens,genes,weights)
%gene-level PCA 

%inputs
%fc_array: matrix of log 2 fold change values of each gene (rows) in each screen (columns), n x k matrix (n genes, k screens)
%groups: string identifiers of each screen; use same identifier for replicate screens
%screens: string identifiers of every screen; use unique names for replicates
%genes: list of genes being analyzed in the screens
%weights: (optional) martix of weights for each gene in each screen, n x k matrix (n genes, k screens)

%outputs
%gl_coefficient: coefficients for each of the principal components, k x q matrix (k screens, q principal components)
%gl_score: score for each gene each for each principal component, n x q matrix (n genes, q principal components)
%gl_explained: % variance explained by each principal component, descending order starting from PC1, q x 1 vector 
%figure 1: coefficients for the first 4 principal components from the gene-level PCA
%figure 2: percentage variance explained by each principal component
%figrue 3: heatmap of the log 2 fold change values of the genes within the
%lowest 1% gene-level PC1 scores

%normalization
nan_id = isnan(fc_array);
nan_id = sum(nan_id,2);
fc_mod = fc_array(nan_id == 0,:);
fc_norm = zscore(fc_mod);

%PCA with or without weights
if nargin == 4
    [coefficient, gl_score,~,~,explained] = pca(fc_norm,'Centered', 'off');
elseif nargin == 5
    weights_mod = weights(nan_id == 0,:);
    weights_screens = sum(weights_mod);
    weights_genes = sum(weights_mod,2);
    [coefficient, gl_score, ~, ~, explained] = pca(fc_norm,'Centered','off','Weights',weights_genes, 'VariableWeights',weights_screens);
else
    disp 'Too few inputs'
    return
end

[~,~,iug] = unique(groups,'stable');
cnt_uni_groups = accumarray(iug,1);
numgroups = length(cnt_uni_groups);

transition = [];
for k = 1:length(cnt_uni_groups)
    if k == 1
        transition(k) = cnt_uni_groups(k);
    else
        transition(k) = transition(k-1)+cnt_uni_groups(k);
    end
end

map = colormap('lines');

pcs = 4;

%Plotting coefficients for the first 4 principal components from the gene-level PCA
figure(1)
for j = 1:pcs
    a = subplot(pcs,1,j);
    b = bar(a, coefficient(:,j));
    b.FaceColor = 'flat';
    title(['Coefficients for PC' num2str(j)]);
    ylabel('Coefficient')
    box('off')
    xticklabels(groups)
    xtickangle(45)
    for i = 1:numgroups
        if i == 1
            b.CData(1:transition(i),:) = repmat(map(i,:),[cnt_uni_groups(i) 1]);
        else
            b.CData(transition(i-1)+1:transition(i),:) = repmat(map(i,:),[cnt_uni_groups(i) 1]);
        end
    end
end
suptitle('Gene-level PCA')

%Plotting the percentage variance explained by each principal component
figure(2)
bar(explained, 'FaceColor','k');
ylabel('Percentage of Variance Explained')
xlabel('Gene-level Principal Components')
box('off')
title('Gene-level PCA')
ylim([0 100])
text(1:length(explained), explained, num2str(round(explained(:,1),0)),'HorizontalAlignment', 'center', 'VerticalAlignment','bottom');

gl_coefficient = coefficient;
gl_explained = explained;

%Plotting heatmap - log 2 fold change values of genes with lowest 1% of PC1 scores
T = array2table(fc_norm);
genes_mod = genes(nan_id == 0,1);
T.genes = genes_mod;
T.PC1 = double(gl_score(:,1));

numscreens = size(fc_norm,2);
lowest = round(0.01*size(T,1));
Ts = sortrows(T,'PC1');
lowpc1_l2fc = table2array(Ts(1:lowest,1:numscreens));
lowpc1_genes = table2cell(Ts(1:lowest, 'genes'));

map = colormap('jet');
nan_col = nan([lowest 1]);

hm_array = [];
hm_screens = {};

if numgroups > 1
    for i=1:numgroups
        if i == 1
            hm_array = [lowpc1_l2fc(:,1:transition(i)) nan_col lowpc1_l2fc(:,transition(i)+1:end)];
            hm_screens = [screens(1:transition(i)) '.' screens(transition(i)+1:end)];
        else
            hm_array = [hm_array(:,1:transition(i)+i-1) nan_col hm_array(:,transition(i)+i:end)];
            hm_screens = [hm_screens(1:transition(i)+i-1) repmat('.',[1 i]) hm_screens(transition(i)+i:end)];
        end
    end
else
    figure (3)
    heatmap(lowpc1_l2fc, 'XData', 'screens', 'YData', lowpc1_genes, 'Colormap', map(1:40,:), 'CellLabelColor', 'none', 'ColorLimits',[-7 1], 'ColorbarVisible','on', 'MissingDataColor', [1 1 1],'MissingDataLabel', '', 'GridVisible', 'off')
    title("L2FC values of genes with low PC1 scores");
end

figure(3)
heatmap(hm_array, 'XData', hm_screens, 'YData', lowpc1_genes, 'Colormap', map(1:40,:), 'CellLabelColor', 'none', 'ColorLimits',[-7 1], 'ColorbarVisible','on', 'MissingDataColor', [1 1 1],'MissingDataLabel', '', 'GridVisible', 'off')
title("Gene-level PCA: Genes with low PC1 scores");

end

