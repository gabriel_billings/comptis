function [sl_score, sl_explained] = screen_level_pca(fc_array, groups, screens, weights)
%screen-level PCA 

%inputs
%fc_array: matrix of log 2 fold change values of each gene (rows) in each screen (columns), n x k matrix (n genes, k screens)
%groups: string identifiers of each type of screen; use same identifier for replicate screens
%screens: string identifiers of every screen; use unique names for replicates
%weights: (optional) martix of weights for each gene in each screen, n x k matrix (n genes, k screens)

%outputs
%sl_score: score for each screen for each principal component, k x q matrix (k screens, q principal components)
%sl_explained: % variance explained by each principal component, descending order starting from PC1, q x 1 vector 
%figure 1: PCA plot - screen-level PCA plotting PC1 and PC2 values for each
%screen
%figure 2: dendrogram of agglomerative hierarchical clustering


%normalization
nan_id = isnan(fc_array);
nan_id = sum(nan_id,2);
fc_mod = fc_array(nan_id == 0,:);
fc_norm = zscore(fc_mod);

%PCA with or without weights
if nargin == 3
    [~, score, ~, ~, explained] = pca(transpose(fc_norm),'Centered', 'off');
elseif nargin == 4
    weights_mod = weights(nan_id == 0,:);
    weights_screens = sum(weights_mod);
    weights_genes = sum(weights_mod,2);
    [~, score, ~, ~, explained] = pca(transpose(fc_norm),'Centered','off','Weights',weights_screens, 'VariableWeights',weights_genes);
else
    disp 'Too few inputs'
    return
end

map = colormap('lines');

%Plotting screen-level PCA - PC1 vs PC2
figure(1)
gscatter(score(:,1),score(:,2), char(groups), map);
title('Screen-level PCA');
xlabel(['PC1 ' num2str(round(explained(1,1),0)) '%']);
ylabel(['PC2 ' num2str(round(explained(2,1),0)) '%']);
legend('Location', 'bestoutside');
legend('boxoff');

%Plotting dendrogram of agglomerative hierarchical clustering
figure(2)
d = pdist(transpose(fc_norm),'euclidean');
tree = linkage(d,'ward');
order = optimalleaforder(tree, d);
gram = dendrogram(tree,0,'labels', screens, 'orientation', 'left', 'reorder', fliplr(order));
set(gram, 'LineWidth', 2, 'Color','k');
title('Hierarchical Clustering dendrogram')

sl_score = score;
sl_explained = explained;

end


