function [issdfcp_report, genes_volcano, genes_fc, genes_weights] = get_pca_inputs(controlsims, sample, uniqueindices, gene_identifier)
format ShortG                                                              %dictates that values will be reported up to 5 digits

%This script performs a comparison of sample data against the
%simulated input data and generates 4 outputs:
%[issdfcp_report] [genes_volcano] [genes_fc] and [genes_weights]

%[issdfcp_report] contains data for all loci:
%[mean # of informative TA sites, standard deviation Log2FC, mean Log2FC, inverse mean MWU P-val]

%[genes_volcano] contains data necessary to make the traditional volcano plot for all genes: 
%[mean Log2FC, inverse mean MWU P-val]

%[genes_fc] and [genes_weights] are used for downstream PCA
%[genes_fc] is just the [mean Log2FC] measurement for each gene
%[genes_weights] is a weight for each gene equivalent to (1 / standard deviation of similarly sized genes)^2


%Part 1: Comparison of sample data against simuladed input data.
%Identifies "Informative Sites" - TA sites that have sustained an
%insertion in either the sample or the simulated input
%extracts read counts at all informative TA sites and tabulates:
    %n: # of informative TA sites per locus
    %sum_controlsim_reads: # of reads per locus in simulated input
    %sum_sample_reads: # of reads per locus in sample
    %p: MWU P-val comparing simultaed input to sample
    %fc: sum_sample_reads divided by sum_controlsim_reads, (imputes 1 read if either value = 0)

[~,col] = size(controlsims);                                               %determines # of simulated inputs in controlsims
sample_array = repmat(sample, 1, col);                                     %expands sample to match size of controlsims
is_array = sample_array + controlsims;                                     %creates is_array: Informative Sites are non-zero, all other TA sites = 0 
for m = 1:col                                                              %outer loop, moves to the next comparison of sample_array against simulated_input after completing all loci
    for i = 1:length(uniqueindices)                                        %inner loop, procedes through each locus, defined by uniqueindices, within a comparison of sample_array against simulated_input
            X = [];                                                        %creates an empty array that will be populated with read counts at informative sites from simulated_input             
            Y = [];                                                        %creates an empty array that will be populated with read counts at informative sites from sample
            for j = uniqueindices(i,1):uniqueindices(i,2)    
                if is_array(j,m) > 0
                    X = [X; controlsims(j,m)];                             %uses an append function to populate X with simulated_input read counts at informative_sites
                    Y = [Y; sample_array(j,m)];                            %uses an append function to populate Y with sample read counts at informative_sites
                end
            end
            if isempty(X) == 1 && isempty(Y) == 1                          %if there are no informative sites in the locus:
                n(i,m) = 0;                                                %returns '0' for n
                sum_controlsim_reads(i,m) = -1;                            %returns '-1' for sum_controlsim_reads
                sum_sample_reads(i,m) = -1;                                %returns '-1' for sum_sample_reads
                p(i,m) = 0/0;                                              %returns 'NaN' for p
                fc(i,m) = 0/0;                                             %returns 'NaN' for fc
            else
                n(i,m) = length(X+Y);                                      %n = # of informative_sites in the locus
                sum_controlsim_reads(i,m) = sum(X);                        %sum_controlsim_reads = sum of X (this retains 0s)
                sum_sample_reads(i,m) = sum(Y);                            %sum_sample_reads = sum of Y (this retains 0s)
                p(i,m) = ranksum(X,Y);                                     %performs a MWU test to compare simulated_input (X) and sample (Y) (this retains 0s)
                if sum(X) == 0                                             %these loops impute -1 for all 0s to enable the fold change calculation
                    sum_controlsim_reads(i,m) = -1;
                else
                    sum_controlsim_reads(i,m) = sum(X);
                end    
                if sum(Y) == 0 
                    sum_sample_reads(i,m) = -1;
                else
                    sum_sample_reads(i,m) = sum(Y);
                end
                fc(i,m) = sum(abs(sum_sample_reads(i,m)))./sum(abs(sum_controlsim_reads(i,m)));    %calculates the fold change (-1 is converted to 1 with abs(X))                                                                
            end
    end
end

%Part 2: Organize results
[avg_n] = mean(n,2);                                                       %calculates the average number of informative sites across iterations
[log2fc] = log2(fc);                                                       %log2 transforms the fold change calculations
[meanlog2fc] = nanmean(log2fc,2);                                          %calculates the average log2 fold change value per locus, for volcano/PCA
[sd_log2fc] = nanstd(log2fc,0,2);                                          %calculates the std dev log2 fold change value per locus, for PCA
[inverse_avg_p] = 1./nanmean(p,2);                                         %calculates the inverse Mean MWU P-val per locus, for volcano
[issdfcp_report] = [avg_n,sd_log2fc,meanlog2fc,inverse_avg_p];             %aggregates relevant data for all loci

%Part 3: Output results
%extract data from issdfcp_report for all genes
genes_n = issdfcp_report(gene_identifier==1,1);
genes_sd = issdfcp_report(gene_identifier==1,2);
genes_fc = issdfcp_report(gene_identifier==1,3);
genes_p = issdfcp_report(gene_identifier==1,4);
genes_volcano = horzcat(genes_fc,genes_p);

%Part 4: Get PCA weights
%Discard genes with 0 informative sites from the [informative sites] and 
%[standard deviation log2fc] arrays prior to fitting
nzgenes_n = genes_n(genes_n>0,1);
nzgenes_sd = genes_sd(genes_n>0,1);

%Fit a power function to nonzero data
[nzpower,gof] = fit(nzgenes_n,nzgenes_sd,'power1','Robust','Bisquare');                         
[coeffvals] = coeffvalues(nzpower);
a_coeff = coeffvals(1,1);
b_coeff = coeffvals(1,2);

%Calculate weights based on the fitted power function
inverse_imputed_sd = 1./(a_coeff.*(genes_n.^b_coeff));
[genes_weights] = (inverse_imputed_sd).^2;
genes_weights(genes_weights==0)=0.001;                                     %the PCA command in matlab can't accomodate weight = 0, so impute 0.001
genes_weights(isnan(genes_weights)==1)=0.001;

%overlay the power function (w/95% confidence intervals) onto the fitted
%data. The axes are determined by the core 98% of X and Y data, respectively 
hold off
scatter(nzgenes_n,nzgenes_sd,4,'k','filled');
%set(gca,'XScale','log');                                                  %uncomment these lines to plot on a log scale                                                   
%set(gca, 'YScale','log');                                                 %uncomment these lines to plot on a log scale
xmin=prctile(nzgenes_n,1);
xmax=prctile(nzgenes_n,99);
ymin=prctile(nzgenes_sd,1);
ymax=prctile(nzgenes_sd,99);
axis([xmin,xmax,ymin,ymax]);
hold on;

%Retrieve the upper and lower bounds of the 95% coefficient CIs
coeff_ci = confint(nzpower);
n=0:max(nzgenes_n);
plot(n,(a_coeff.*(n.^b_coeff)),'r','linewidth',2);
a_min=coeff_ci(1,1);
a_max=coeff_ci(2,1);
b_min=coeff_ci(1,2);
b_max=coeff_ci(2,2);
plot(n,a_min*n.^b_min,'b');
plot(n,a_max*n.^b_max,'b');
xlabel('Mean informative TA sites across simulations');                 
ylabel('Standard Deviation of Log2(FC) across simulations');            
title(['Power Law Fit, R^2=',num2str(gof.rsquare)]);



  
      
                                                                            
                                                                   
                                                                            
                                                                     