
### Contents ###

* shortsims.m - generates 100 control input libraries by simulating bottleneck 
* get_pca_inputs_weights.m - calculates l2fc and stdev for each gene, calculates weights using power law fit curve
* screen_level_pca.m - runs screen level PCA (weights optional) plots: (1) PCA plot and (2) clustering dendrogram
* gene_level_pca.m - runs gene level PCA (weights optional) plots: (1) PC coefficients, (2) % explained for the principal components, and (3) heatmap of genes with lowest 1% PC1 scores
* Sample data: Data from the vibrio comparison (VP, VC Peru, VC Haiti) for others to use to learn how to use Screen/Gene-level PCA and clustering. 

