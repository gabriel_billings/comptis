function [controlsims] = shortsims(sample_reads,input_reads,boots)
%sample_reads is the vector of your lower complexity library
%input_reads is the vector of your higher complexity library

sum_sample_reads = sum(sample_reads);                                                               %determines the total number of reads for each library
sum_input_reads = sum(input_reads);

ta_prop_sample = sample_reads./sum_sample_reads;                                                    %determines the proportional abundance of mutants for each library
ta_prop_input = input_reads./sum_input_reads;

ta_prop_difference = length(sample_reads(sample_reads~=0))/length(input_reads(input_reads~=0));     %determines the difference in the number of unique mutants between low complexity vs. high complexity...
                                                                                                    %for approximating the bottleneck
                                                                                                    
%reads_per_sample_ta = sum_sample_reads/length(sample_reads(sample_reads~=0));                      %determines the reads per mutant for each library
%reads_per_input_ta = sum_input_reads/length(input_reads(input_reads~=0));
%if reads_per_sample_ta > reads_per_input_ta
%    sum_sample_reads = sum_sample_reads*(reads_per_input_ta/reads_per_sample_ta);                  %if the lower complexity library is sequenced deeper than the higher complexity library...
%else                                                                                               %then the total number of reads in the lower complexity library is scaled down so that...
%    sum_sample_reads = sum_sample_reads;                                                           %both libraries have the same average reads per insertion (avoids multiplicative upscaling)...
%end                                                                                                %if the higher complexity library is sequenced deepr than no changes are made

norm_ta_prop_input = ta_prop_input.*ta_prop_difference;                                             %the proportional abundance of each mutant in the higher complexity library is reduced by the bottleneck approximation
norm_ta_prop_input(length(norm_ta_prop_input)+1,1)=1-ta_prop_difference;                            %an extra row is added with 1-bottleneck correction

multinom_input_sample = mnrnd(sum_sample_reads,norm_ta_prop_input,boots);                           %rolls a k-sided weighted dice (k=number of mutants in higher complexity library, weighted bottleneck-adjusted proportional abundance)...
multinom_input_sample = multinom_input_sample';                                                     %n-times (n=total number of sequence reads in the lower complexity library (this may have been adjusted  by line if lower complexity... 
                                                                                                    %library has deeper sequencing) and reorients the array

multisum = sum(multinom_input_sample);                                                              %calculates the total number of results from the multinomial distribution
difference = multisum-multinom_input_sample(length(multinom_input_sample),1);                       %figures out how many of the results from the multinomial distrbuition are in the bottleneck correction row 
correction = repmat((sum_sample_reads./difference),length(multinom_input_sample),1);                %calculates a scaling factor so that you can throw away the bottleneck correction row, and scale up all other non-zero rows accordingly...
correctedinput = multinom_input_sample.*correction;                                                 %so that you get the same total number of reads, and perform this across all boots
controlsims = correctedinput(1:length(input_reads),:);
end